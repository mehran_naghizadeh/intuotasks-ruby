#!/usr/bin/env ruby
# frozen_string_literal: true

$LOOKUP = {
  '00' => { carry: '0', sum: '0' },
  '01' => { carry: '0', sum: '1' },
  '02' => { carry: '0', sum: '2' },
  '03' => { carry: '0', sum: '3' },
  '04' => { carry: '0', sum: '4' },
  '05' => { carry: '0', sum: '5' },
  '06' => { carry: '0', sum: '6' },
  '07' => { carry: '0', sum: '7' },
  '08' => { carry: '0', sum: '8' },
  '09' => { carry: '0', sum: '9' },

  '10' => { carry: '0', sum: '1' },
  '11' => { carry: '0', sum: '2' },
  '12' => { carry: '0', sum: '3' },
  '13' => { carry: '0', sum: '4' },
  '14' => { carry: '0', sum: '5' },
  '15' => { carry: '0', sum: '6' },
  '16' => { carry: '0', sum: '7' },
  '17' => { carry: '0', sum: '8' },
  '18' => { carry: '0', sum: '9' },
  '19' => { carry: '1', sum: '0' },

  '20' => { carry: '0', sum: '2' },
  '21' => { carry: '0', sum: '3' },
  '22' => { carry: '0', sum: '4' },
  '23' => { carry: '0', sum: '5' },
  '24' => { carry: '0', sum: '6' },
  '25' => { carry: '0', sum: '7' },
  '26' => { carry: '0', sum: '8' },
  '27' => { carry: '0', sum: '9' },
  '28' => { carry: '1', sum: '0' },
  '29' => { carry: '1', sum: '1' },

  '30' => { carry: '0', sum: '3' },
  '31' => { carry: '0', sum: '4' },
  '32' => { carry: '0', sum: '5' },
  '33' => { carry: '0', sum: '6' },
  '34' => { carry: '0', sum: '7' },
  '35' => { carry: '0', sum: '8' },
  '36' => { carry: '0', sum: '9' },
  '37' => { carry: '1', sum: '0' },
  '38' => { carry: '1', sum: '1' },
  '39' => { carry: '1', sum: '2' },

  '40' => { carry: '0', sum: '4' },
  '41' => { carry: '0', sum: '5' },
  '42' => { carry: '0', sum: '6' },
  '43' => { carry: '0', sum: '7' },
  '44' => { carry: '0', sum: '8' },
  '45' => { carry: '0', sum: '9' },
  '46' => { carry: '1', sum: '0' },
  '47' => { carry: '1', sum: '1' },
  '48' => { carry: '1', sum: '2' },
  '49' => { carry: '1', sum: '3' },

  '50' => { carry: '0', sum: '5' },
  '51' => { carry: '0', sum: '6' },
  '52' => { carry: '0', sum: '7' },
  '53' => { carry: '0', sum: '8' },
  '54' => { carry: '0', sum: '9' },
  '55' => { carry: '1', sum: '0' },
  '56' => { carry: '1', sum: '1' },
  '57' => { carry: '1', sum: '2' },
  '58' => { carry: '1', sum: '3' },
  '59' => { carry: '1', sum: '4' },

  '60' => { carry: '0', sum: '6' },
  '61' => { carry: '0', sum: '7' },
  '62' => { carry: '0', sum: '8' },
  '63' => { carry: '0', sum: '9' },
  '64' => { carry: '1', sum: '0' },
  '65' => { carry: '1', sum: '1' },
  '66' => { carry: '1', sum: '2' },
  '67' => { carry: '1', sum: '3' },
  '68' => { carry: '1', sum: '4' },
  '69' => { carry: '1', sum: '5' },

  '70' => { carry: '0', sum: '7' },
  '71' => { carry: '0', sum: '8' },
  '72' => { carry: '0', sum: '9' },
  '73' => { carry: '1', sum: '0' },
  '74' => { carry: '1', sum: '1' },
  '75' => { carry: '1', sum: '2' },
  '76' => { carry: '1', sum: '3' },
  '77' => { carry: '1', sum: '4' },
  '78' => { carry: '1', sum: '5' },
  '79' => { carry: '1', sum: '6' },

  '80' => { carry: '0', sum: '8' },
  '81' => { carry: '0', sum: '9' },
  '82' => { carry: '1', sum: '0' },
  '83' => { carry: '1', sum: '1' },
  '84' => { carry: '1', sum: '2' },
  '85' => { carry: '1', sum: '3' },
  '86' => { carry: '1', sum: '4' },
  '87' => { carry: '1', sum: '5' },
  '88' => { carry: '1', sum: '6' },
  '89' => { carry: '1', sum: '7' },

  '90' => { carry: '0', sum: '9' },
  '91' => { carry: '1', sum: '0' },
  '92' => { carry: '1', sum: '1' },
  '93' => { carry: '1', sum: '2' },
  '94' => { carry: '1', sum: '3' },
  '95' => { carry: '1', sum: '4' },
  '96' => { carry: '1', sum: '5' },
  '97' => { carry: '1', sum: '6' },
  '98' => { carry: '1', sum: '7' },
  '99' => { carry: '1', sum: '8' }
}

def solution(num1, num2)
  m = num1.chars
  n = num2.chars

  carry = 0
  result = ''

  until m.empty? && n.empty?
    a = m.pop || '0'
    b = n.pop || '0'

    a_plus_carry = $LOOKUP["#{a}#{carry}"]
    begin
      b_plus_a_plus_carry = $LOOKUP["#{b}#{a_plus_carry[:sum]}"]
    rescue
      return "Illegal input, please don't use non ASCII characters."
    end

    carry = $LOOKUP["#{b_plus_a_plus_carry[:carry]}#{a_plus_carry[:carry]}"][:sum]

    result = b_plus_a_plus_carry[:sum] + result
  end

  (carry == '1' ? '1' : '') + result
end


STOP_COMMAND = 'abort'

loop do
  print "Enter the first number(#{STOP_COMMAND} to exit): "
  a = gets.chomp
  abort('Good bye!') if a == STOP_COMMAND

  print "Enter the second number(#{STOP_COMMAND} to exit): "
  b = gets.chomp
  abort('Good bye!') if b == STOP_COMMAND

  puts solution(a, b)
end
