#!/usr/bin/env ruby
# frozen_string_literal: true

# define the constants
$RIGHT = 'Rip the top row from left to right'
$DOWN = 'Rip the right column from top to bottom'
$LEFT = 'Rip the bottom column from right to left'
$UP = 'Rip the left column from bottom to top'

$DIRECTIONS = [$RIGHT, $DOWN, $LEFT, $UP]

def base_array(size)
  result = []
  size.times do |t|
    result.push([*(1 + t * size)..((1 + t) * size)])
  end
  result
end

def rip(arr, direction)
  r = []
  case direction
  when $RIGHT then r = arr.shift
  when $LEFT then r = arr.pop.reverse
  when $DOWN then r = arr.map(&:pop)
  when $UP then r = arr.map(&:shift).reverse
  end

  r
end

def solution(arr)
  direction_index = 0
  result = []
  until arr.empty?
    result += rip(arr, $DIRECTIONS[direction_index])
    direction_index = (direction_index + 1) % $DIRECTIONS.length
  end

  result
end

STOP_COMMAND = 'e'

loop do
  print "Give me the size of array (#{STOP_COMMAND} for exit): "
  user_input = gets.chomp
  if user_input == STOP_COMMAND
    abort('Good bye')
  else
    n = user_input.to_i
    if n.zero?
      puts "#{user_input} is not valid"
    else
      puts solution(base_array(n)).join(' ')
    end
  end
end
