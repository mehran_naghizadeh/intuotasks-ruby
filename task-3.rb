#!/usr/bin/env ruby
# frozen_string_literal: true

def solution(str, rotation)
  str.each_byte.map do |b|
    case b
    when 97..122 then (97 + (b - 97 + rotation) % 26).chr
    when 65..90 then (65 + (b - 65 + rotation) % 26).chr
    else; b.chr
    end
  end
     .join
end

STOP_COMMAND = '#!abort'

loop do
  print "Enter a string(#{STOP_COMMAND} for exit): "
  str = gets.chomp

  if str == STOP_COMMAND
    abort('Good bye!')
  else
    print 'How many times do you want to rotate it? '
    k = gets.chomp.to_i

    puts solution(str, k)
  end
end

