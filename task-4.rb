#!/usr/bin/env ruby
# frozen_string_literal: true

def factorial(num)
  [*(1..num)].reduce { |mem, x| mem * x }
end

def partition(arr)
  arr.group_by(&:itself).transform_values(&:size)
end

def permutations(arr)
  factorial(arr.length) / partition(arr).each_value.reduce { |memo, value| memo * factorial(value) }
end

def smallers_cnt(str)
  if str.length == 2
    str[0] > str[1] ? 1 : 0
  else
    num = 0
    head = str[0]
    tail = str[1..(str.length - 1)]
    $sorted_letters.select { |char| char < head && tail.include?(char) }.each do |char|
      num += permutations(tail.sub(char, head).chars)
    end
    num
  end
end

def solution(word)
  smallers = 0
  (word.length - 2).downto 0 do |index|
    tail = word[index..(word.length - 1)]
    smallers += smallers_cnt(tail)
  end

  smallers + 1
end

STOP_COMMAND = '#!abort'
$sorted_letters = []

loop do
  print "Enter a string(#{STOP_COMMAND} for exit): "
  str = gets.chomp

  if str == STOP_COMMAND
    abort('Good bye!')
  else
    $sorted_letters = str.chars.uniq.sort
    puts solution(str)
  end
end
